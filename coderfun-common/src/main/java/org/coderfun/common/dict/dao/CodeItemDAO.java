package org.coderfun.common.dict.dao;

import org.coderfun.common.dict.entity.CodeItem;

import klg.common.dataaccess.BaseRepository;

public interface CodeItemDAO extends BaseRepository<CodeItem, Long> {

}
